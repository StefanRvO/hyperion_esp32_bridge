#include "debug_led.h"
#include "driver/ledc.h"
#include "esp32_digital_led_lib.h"

static const gpio_num_t PCB_LED_PIN = (gpio_num_t)2;
static bool state = false;


void debug_led_init(void)
{
    ledc_timer_config_t ledc_timer = {
        .duty_resolution = LEDC_TIMER_8_BIT,     // resolution of PWM duty
        .freq_hz         = 100000,               // frequency of PWM signal
        .speed_mode      = LEDC_HIGH_SPEED_MODE, // timer mode
        .timer_num       = LEDC_TIMER_0,         // timer index
        .clk_cfg         = LEDC_AUTO_CLK,        // Auto select the source clock
    };
    ledc_timer_config(&ledc_timer);

    ledc_channel_config_t ledc_channel = {
        .channel    = LEDC_CHANNEL_0,
        .duty       = 0,
        .gpio_num   = PCB_LED_PIN,
        .speed_mode = LEDC_HIGH_SPEED_MODE,
        .hpoint     = 0,
        .timer_sel  = LEDC_TIMER_0
    };

    ledc_channel_config(&ledc_channel);
    debug_led_off();
}

void debug_led_toggle(void)
{
    state = !state;
    if(state)
    {
        debug_led_on();
    }
    else
    {
        debug_led_off();
    }
}

void debug_led_on(void)
{
    state = true;
    ledc_set_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, 250);
    ledc_update_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0);
}

void debug_led_off(void)
{
    state = false;
    ledc_set_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0, 255);
    ledc_update_duty(LEDC_HIGH_SPEED_MODE, LEDC_CHANNEL_0);
}

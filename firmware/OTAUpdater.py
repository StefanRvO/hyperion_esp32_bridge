import argparse
import threading
import time
import struct
from tqdm import tqdm
import socket

class OTAUpgrader(threading.Thread):
    def __init__(self, ip, path):
        super().__init__(daemon=True)

        with open(path, "rb") as f:
            self.bin_data = f.read()

        self.ip        = ip
        self.offset    = 0
        self.sock = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        self.sock.connect((self.ip, 5001))

    def upgrade(self):
        self.start()
        self.join()

    def send_next_chunk(self):
        payload = self.bin_data[self.offset:self.offset + 4096]
        data = struct.pack("<III", len(self.bin_data),
                                   self.offset,
                                   len(payload))

        self.sock.send(data + payload)
        self.offset += len(payload)

        return len(payload)

    def run(self):
        pbar = tqdm(total=len(self.bin_data), unit = "bytes")

        while self.offset != len(self.bin_data):
            pbar.update(self.send_next_chunk())
        pbar.close()

    def receive_thread(self):
        pass
    
class OTACompressedUploader(OTAUpgrader):
    def send_next_chunk(self):
        return super().send_next_chunk()

    def run(self):
        return super().run()


def __main__():
    parser = argparse.ArgumentParser(description='Flash ota upgrade to device')
    parser.add_argument('--ip', help='IP of destination device', required=True)
    parser.add_argument('-fw', '--firmware', help="firmware to flash", required=True)
    args = parser.parse_args()

    upgrader = OTAUpgrader(args.ip, args.firmware)
    upgrader.upgrade()


if __name__ == "__main__":
    __main__()
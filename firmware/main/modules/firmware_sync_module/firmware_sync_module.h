#pragma once
#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

#ifdef __cplusplus
extern "C" {
#endif

void firmware_sync_module_init(void);
bool firmware_sync_module_is_sending(void);
uint32_t firmware_sync_module_get_receive_offset(void);
void firmware_sync_module_send_data_step(void);
void firmware_sync_module_send_progress_step(void);
const uint8_t *firmware_sync_module_get_sha256(void);
void firmware_sync_module_ota_erase(void);
void firmware_sync_module_write_firmware(uint8_t* buf, size_t offset, size_t size);
uint32_t firmware_sync_get_bytes_left_to_receive(void);
float firmware_sync_get_synced_part(void);

#ifdef __cplusplus
}
#endif
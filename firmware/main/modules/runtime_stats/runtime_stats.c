/**
 * @brief Timed printing system information
 */
#include "esp_err.h"
#include "esp_log.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>


static TickType_t next_print_time = 0;

void print_runtime_stats()
{
    TickType_t current_time = xTaskGetTickCount();
    if(current_time > next_print_time)
    {
        size_t heap = xPortGetFreeHeapSize();
        ESP_LOGI("HEAP", "free: %d", heap);
        char buf[1000];
        vTaskGetRunTimeStats( buf );
        printf("%s\n", buf);
        next_print_time = current_time + 5000;
    }
}
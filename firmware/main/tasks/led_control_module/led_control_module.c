#include "esp32_digital_led_lib.h"

#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include <string.h>
#include "esp_log.h"
#include "modules/debug_led/debug_led.h"
#include "modules/runtime_stats/runtime_stats.h"

#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)

#define LED_PIN 27

static TickType_t last_update = 0;

strand_t  strands[1];
strand_t* strand_ptrs[1];
strand_t* strand_hyperion = &strands[0];
strand_t* strand_debug    = &strands[1];

void led_task(void *taskdata)
{
    memset(strands, 0x00, sizeof(strands));
    strand_hyperion->rmtChannel  = 0;
    strand_hyperion->gpioNum     = LED_PIN;
    strand_hyperion->ledType     = LED_WS2812B_V2;
    strand_hyperion->brightLimit = 255;
    strand_hyperion->numPixels   = 500;


    strand_ptrs[0] = strand_hyperion;

    digitalLeds_initDriver();
    digitalLeds_addStrands(strand_ptrs, 1);

    while(true)
    {
        if(xTaskGetTickCount() - last_update > 1000)
        {
            last_update = xTaskGetTickCount();
            ESP_LOGI("LED_CONTROL", "reset leds %d", strands[0].numPixels);
            digitalLeds_resetPixels(strand_ptrs, 1);
            debug_led_off();
        }
        delay_ms(100);
    }
}

void set_led_count(uint32_t count)
{
    if(count != strand_hyperion->numPixels)
    {
        digitalLeds_removeStrands(strand_ptrs, 1);
        strand_hyperion->numPixels = count;
        digitalLeds_addStrands(strand_ptrs, 1);
    }
}

void set_pixel(uint32_t pixel, pixelColor_t color)
{
    strand_hyperion->pixels_actual[pixel] = color;
}

void latch_leds(void)
{
    last_update = xTaskGetTickCount();
    debug_led_toggle();
    if(!digitalLeds_isDrawing())
    {
        digitalLeds_drawPixels(strand_ptrs, 1);
    }
}
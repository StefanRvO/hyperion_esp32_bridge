#include "webserver_task.h"
#include "esp_err.h"
#include "esp_log.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

#include <stdio.h>
#include <stdlib.h>

#include <netdb.h>
#include <netinet/in.h>
#include <string.h>
#include "modules/ota_upgrade_handler/ota_upgrade_handler.h"


__attribute__((unused)) static const char *TAG = "WEB_SERVER";

static void handle_connection(int sock);
static bool read_to_buffer(int sock, void* buf, uint32_t size);

void web_server_task(void *parameters)
{
  int portno = 5001;

  struct sockaddr_in serv_addr, cli_addr;
   
  int sock = socket(AF_INET, SOCK_STREAM, 0);
   
  if (sock < 0) {
    ESP_LOGE(TAG, "ERROR opening socket");
    return;
  }
  
  /* Initialize socket structure */
  bzero((char *) &serv_addr, sizeof(serv_addr));
  
  serv_addr.sin_family = AF_INET;
  serv_addr.sin_addr.s_addr = INADDR_ANY;
  serv_addr.sin_port = htons(portno);
  
  /* Now bind the host address using bind() call.*/
  if (bind(sock, (struct sockaddr *) &serv_addr, sizeof(serv_addr)) < 0) {
    ESP_LOGE(TAG, "ERROR on binding\n");
    return;
  }
    
  while(true)
  {
    listen(sock, 5);
    socklen_t clilen = sizeof(cli_addr);
    
    /* Accept actual connection from the client */
    int client_sock = accept(sock, (struct sockaddr *)&cli_addr, &clilen);
    
    if (client_sock < 0) {
        ESP_LOGE(TAG, "ERROR on accept");
    }
    handle_connection(client_sock);
    close(client_sock);
  }
}

void handle_connection(int sock)
{
  while(true)
  {
    ota_upgrade_data_t ota_data;
    //Read header
    if(!read_to_buffer(sock, &ota_data, 12))
    {
      return;
    }

    if(ota_data.size > sizeof(ota_data.data))
    {
        ESP_LOGE(TAG, "Size was larger than allowed: %d -- %d", ota_data.size, sizeof(ota_data.data));
        return;
    }

    if(!read_to_buffer(sock, &ota_data.data, ota_data.size))
    {
      return;
    }
    if(!ota_upgrade_handler_handle(&ota_data))
    {
      return;
    }
  }
}

bool read_to_buffer(int sock, void* buf, uint32_t size)
{
  uint32_t read_cnt = 0;
  while(read_cnt != size)
  {
    int n = read(sock, buf, size - read_cnt);
    if(n <= 0)
    {
      printf("Socker closed\n");
      return false;
    }
    buf += n;
    read_cnt += n;
  }
  return true;
}
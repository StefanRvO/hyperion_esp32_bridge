#pragma once

#include <stdint.h>
#include "esp32_digital_led_lib.h"

void led_task(void *taskdata);
void set_pixel(uint32_t pixel, pixelColor_t color);
void set_led_count(uint32_t count);
void latch_leds(void);
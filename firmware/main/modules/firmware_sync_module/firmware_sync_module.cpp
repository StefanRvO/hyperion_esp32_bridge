#include "firmware_sync_module.h"
#include "esp_err.h"
#include "esp_log.h"

#include "libs/sync_lib/sync_lib.hpp"

class FirmwareSyncer : public PartitionSyncer
{
    public:
        FirmwareSyncer(): PartitionSyncer("FirmwareSync")
        {}

        virtual const esp_partition_t* get_target_partition() const
        {
            return esp_ota_get_next_update_partition(NULL);
        }
        virtual const esp_partition_t* get_current_partition() const
        {
            return esp_ota_get_running_partition();
        }
        virtual void handle_done_receiving()
        {
            ESP_ERROR_CHECK(esp_ota_set_boot_partition(get_target_partition()));
            esp_restart();
        }
};

static FirmwareSyncer *syncer;

void firmware_sync_module_init(void)
{
    syncer = new FirmwareSyncer();
    syncer->init();
}

bool firmware_sync_module_is_sending(void)
{
    return syncer->get_next_send_offset() != 0;
}

uint32_t firmware_sync_module_get_receive_offset(void)
{
    return syncer->get_next_receive_offset();
}

void firmware_sync_module_write_firmware(uint8_t* buf, size_t offset, size_t size)
{
    syncer->write_chunk(buf, offset, size);
}

void firmware_sync_module_ota_erase()
{
    syncer->erase_partition();
}

const uint8_t *firmware_sync_module_get_sha256(void)
{
    return syncer->sha256;
}

uint32_t firmware_sync_get_bytes_left_to_receive(void)
{
    return syncer->missing_recv_chunks.get_total_size();
}

float firmware_sync_get_synced_part(void)
{
    return 1 - (float)syncer->missing_recv_chunks.get_total_size() / (float) syncer->get_target_partition_size();
}
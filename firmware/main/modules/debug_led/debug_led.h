#pragma once

void debug_led_init(void);
void debug_led_toggle(void);
void debug_led_on(void);
void debug_led_off(void);

#include "tpm2net_decoder.h"
#include "tasks/led_control_module/led_control_module.h"
#include <string.h>
#include <sys/param.h>
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"
#include "esp_system.h"
#include "esp_wifi.h"
#include "esp_event.h"
#include "esp_log.h"
#include "nvs_flash.h"
#include "esp_netif.h"

#include "lwip/err.h"
#include "lwip/sockets.h"
#include "lwip/sys.h"
#include <lwip/netdb.h>

static const char *TAG = "TPM2_DECODER";


#define delay_ms(ms) vTaskDelay((ms) / portTICK_RATE_MS)
#define PORT 12345

static void parse_dataframe(uint8_t* buf, uint32_t buf_len);
static void parse_payload(uint8_t* buf, uint32_t framesize);

static bool header_valid(uint8_t* buf);
static bool footer_valid(uint8_t last_byte);

void tpm2_task(void *taskdata)
{
    while (true)
    {
        uint8_t rx_buffer[3000];
        struct sockaddr_in dest_addr;
        dest_addr.sin_addr.s_addr = htonl(INADDR_ANY);
        dest_addr.sin_family = AF_INET;
        dest_addr.sin_port = htons(PORT);
        int sock = socket(AF_INET, SOCK_DGRAM, IPPROTO_IP);

        ESP_LOGI(TAG, "Socket created");

        int err = bind(sock, (struct sockaddr *)&dest_addr, sizeof(dest_addr));
        if (err < 0) {
            ESP_LOGE(TAG, "Socket unable to bind: errno %d", errno);
            break;
        }
        ESP_LOGI(TAG, "Socket bound, port %d", PORT);

        while(true)
        {
            struct sockaddr_in source_addr;
            socklen_t socklen = sizeof(source_addr);
            int len = recvfrom(sock, rx_buffer, sizeof(rx_buffer) - 1, 0, (struct sockaddr *)&source_addr, &socklen);
            parse_dataframe(rx_buffer, len);
        }
    }
}


void parse_dataframe(uint8_t* buf, uint32_t buf_len)
{
    if(buf_len < 6 || !header_valid(buf) || !footer_valid(*(buf + buf_len - 1)))
    {
        return;
    }
    uint16_t framesize = buf[2];
    framesize <<= 8;
    framesize += buf[3];
    uint8_t frame_cnt = buf[4];
    //Adjust buffer to only hold payload
    buf += 5;
    buf_len -= 6;
    if(framesize > buf_len)
    {
        ESP_LOGE(TAG, "Framesize larger than buffer: %d %d", framesize, buf_len);
    }

    //We only support single frame data for now.
    parse_payload(buf, framesize);
}

void parse_payload(uint8_t* buf, uint32_t framesize)
{
    set_led_count(framesize / 3);
    for(uint32_t i = 0; i < framesize; i+=3)
    {
        pixelColor_t color = {.raw32 = 0};
        color.raw32 <<= 8;
        color.raw32 += buf[i + 0];
        color.raw32 <<= 8;
        color.raw32 += buf[i + 1];
        color.raw32 <<= 8;
        color.raw32 += buf[i + 2];
        set_pixel(i / 3, color);
    }
    latch_leds();
    
}

bool header_valid(uint8_t* buf)
{
    return buf[0] == 0x9C && buf[1] == 0xDA;
}

bool footer_valid(uint8_t last_byte)
{
    return last_byte == 0x36;
}
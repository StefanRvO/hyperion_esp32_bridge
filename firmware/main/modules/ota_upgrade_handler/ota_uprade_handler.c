#include "ota_upgrade_handler.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"

#include "esp_err.h"
#include "esp_log.h"
#include "esp_ota_ops.h"
#include "esp_system.h"
#include <freertos/FreeRTOS.h>
#include <freertos/task.h>

__attribute__((unused)) static const char *TAG = "OTA_UPGRADE";

bool ota_upgrade_handler_handle(ota_upgrade_data_t *msg)
{
    if(msg->offset == 0)
    {
        ESP_LOGI(TAG, "OTA Upgrade started");
        TickType_t t = xTaskGetTickCount();
        firmware_sync_module_ota_erase();
        printf("Erase time: %d\n", xTaskGetTickCount() - t);
    }
    if(msg->offset != firmware_sync_module_get_receive_offset())
    {   
        return false;
    }
    else
    {
        TickType_t t = xTaskGetTickCount();
        firmware_sync_module_write_firmware(msg->data, msg->offset, msg->size);
        printf("write time: %d\n", xTaskGetTickCount() - t);
        if(msg->offset + msg->size >= msg->total_size)
        {
            const esp_partition_t * part = esp_ota_get_next_update_partition(NULL);
            ESP_ERROR_CHECK(esp_ota_set_boot_partition(part));
            esp_restart();
        }
    }  
    return true;
}
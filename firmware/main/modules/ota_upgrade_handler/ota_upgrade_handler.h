#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdbool.h>

typedef struct __attribute__((packed))
{
    uint32_t total_size;
    uint32_t offset;
    uint32_t size;
    uint8_t data[4096];
} ota_upgrade_data_t;


bool ota_upgrade_handler_handle(ota_upgrade_data_t *msg);
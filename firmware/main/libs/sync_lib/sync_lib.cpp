#include "sync_lib.hpp"
#include "esp_err.h"
#include "esp_log.h"
#include <string.h>
#include <algorithm>

SyncedChunks::SyncedChunks(sync_chunk_t* chunks, uint8_t chunk_cnt)
{
    for(uint8_t i = 0; i < chunk_cnt; i++)
    {
        pair<uint32_t, uint32_t> chunk((uint32_t)chunks[i].start, (uint32_t)chunks[i].end);
        this->push_back(chunk);
    }
}

bool SyncedChunks::remove_piece(uint32_t offset, uint32_t size)
{
    for(auto itt = this->begin(); itt != this->end(); itt++)
    {
        auto& chunk = *itt;
        if(offset >= chunk.first && offset <= chunk.second)
        {
            if(offset == chunk.first)
            {
                chunk.first += size;
                if(chunk.first >= chunk.second)
                {
                    this->erase(itt);
                }
            }
            else
            {
                pair<uint32_t, uint32_t> new_chunk(offset + size, chunk.second);
                chunk.second = offset;
                auto new_itt = itt;  
                if(new_chunk.first < new_chunk.second)
                {
                    new_itt++;
                    this->insert(new_itt, new_chunk);
                }
                if(chunk.second <= chunk.first)
                {
                    this->erase(itt);
                    ESP_LOGI("CHUNK", "Removed piece. %d", this->size());
                }
            }
            max_chunks = max(this->size(), max_chunks);
            return true;
        }
    }
    return false;
}

uint32_t SyncedChunks::get_total_size() const
{
    uint32_t sum = 0;
    for(auto itt = this->begin(); itt != this->end(); itt++)
    {
        sum += itt->second - itt->first;
    }
    return sum;
}

uint8_t SyncedChunks::serialize(sync_chunk_t* chunks) const 
{
    uint8_t i = 0;
    auto itt = this->begin();
    for(i = 0, itt = this->begin(); i < MAX_CHUNKS && itt != this->end(); i++, itt++)
    {
        chunks[i].start = itt->first;
        chunks[i].end   = itt->second;
    }
    return i;
}

uint32_t SyncedChunks::get_first_offset() const 
{
    if(this->size())
    {
        return this->begin()->first;
    }
    return 0;
}

void SyncedChunks::reset(uint32_t total_size)
{
    this->clear();
    this->push_back(pair<uint32_t, uint32_t>(0, total_size));
}

void SyncedChunks::splice(const SyncedChunks &chunksToSplice)
{
    this->insert(this->end(), chunksToSplice.begin(), chunksToSplice.end());
    SyncedChunks result;
    auto it = this->begin();
    auto current = *(it)++;
    while (it != this->end()){
       if (current.second >= it->first){
           current.second = std::max(current.second, it->second); 
       } else {
           result.push_back(current);
           current = *(it);
       }
       it++;
    }
    result.push_back(current);
    this->clear();
    this->insert(this->end(), result.begin(), result.end());
    max_chunks = max(this->size(), max_chunks);
}

void SyncedChunks::print_chunks() const
{
    const char* tag = "SyncedChunks";
    ESP_LOGI(tag, "chunks: %d", this->size());
    for(auto itt = this->begin(); itt != this->end(); itt++)
    {
        ESP_LOGI(tag, "%d-%d", itt->first, itt->second);
    }
}
    
void PartitionSyncer::init()
{
    update_sha256();
}

uint32_t PartitionSyncer::read_chunk(uint8_t* buf, size_t offset, size_t size)
{
    if(size == 0)
    {
        return 0;
    }

    const esp_partition_t* part = get_current_partition();
    uint32_t bytes_remaining = part->size - offset;
    ESP_ERROR_CHECK(esp_partition_read(part, offset, buf, bytes_remaining > size ? size : bytes_remaining));
    return bytes_remaining > size ? size : bytes_remaining;
}

uint32_t PartitionSyncer::write_chunk(uint8_t* buf, size_t offset, size_t size)
{
    if(size == 0)
    {
        return 0;
    }

    if(this->missing_recv_chunks.remove_piece(offset, size))
    {
        const esp_partition_t * part = get_target_partition();
        uint32_t bytes_remaining = part->size - offset;
        uint32_t bytes_to_write = bytes_remaining > size ? size : bytes_remaining;
        ESP_ERROR_CHECK(esp_partition_write(part, offset, buf, bytes_to_write));
        return bytes_to_write;
    }
    return 0;
}

void PartitionSyncer::update_sha256()
{
    const esp_partition_t *part = get_current_partition();
    ESP_ERROR_CHECK(esp_partition_get_sha256(part, sha256));
}

const esp_partition_t* PartitionSyncer::get_current_partition() const
{
    return this->get_target_partition();
}

uint32_t PartitionSyncer::get_target_partition_size()
{
    return get_target_partition()->size;
}

uint32_t PartitionSyncer::get_next_receive_offset() const
{
    return missing_recv_chunks.get_first_offset();
}

uint32_t PartitionSyncer::get_next_send_offset() const
{
    return missing_send_chunks.get_first_offset();
}

void PartitionSyncer::erase_partition()
{
    const esp_partition_t * part = get_target_partition();
    ESP_ERROR_CHECK(esp_partition_erase_range(part, 0, part->size));
    missing_recv_chunks.reset(get_target_partition_size());
}
#include "tasks/led_control_module/led_control_module.h"
#include "tasks/tpm2net_decoder/tpm2net_decoder.h"
#include "tasks/webserver_task/webserver_task.h"
#include "modules/wifi_module/wifi_module.h"
#include "modules/debug_led/debug_led.h"
#include "modules/firmware_sync_module/firmware_sync_module.h"

#include "nvs_flash.h"
#include "freertos/FreeRTOS.h"
#include "freertos/task.h"




void app_main()
{
    esp_err_t ret = nvs_flash_init();
    if (ret == ESP_ERR_NVS_NO_FREE_PAGES || ret == ESP_ERR_NVS_NEW_VERSION_FOUND) {
      ESP_ERROR_CHECK(nvs_flash_erase());
      ret = nvs_flash_init();
    }
    ESP_ERROR_CHECK(ret);

    wifi_init_sta();
    debug_led_init();
    firmware_sync_module_init();
    xTaskCreatePinnedToCore(led_task,  "LED_TASK", 10000, NULL, 10, NULL, 1);
    xTaskCreatePinnedToCore(tpm2_task, "tpm2_task", 10000, NULL, 10, NULL, 1);
    xTaskCreatePinnedToCore(web_server_task, "WebServer", 10000, NULL, 10, NULL, 0);
}